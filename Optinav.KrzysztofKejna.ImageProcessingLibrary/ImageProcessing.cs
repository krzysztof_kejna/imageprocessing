﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Optinav.KrzysztofKejna.ImageProcessingLibrary
{
    public interface IImageProcessing
    {
        void Save(Image image, string path);
        Image ToMainColors(Image image);
        Task<Image> ToMainColorsAsync(Image image);
    }

    public class ImageProcessing : IImageProcessing
    {
        public Image ToMainColors(Image image)
        {
            Bitmap picture = new Bitmap(image);

            ChangePixel(picture);

            return (Image)picture;
        }

        public async Task<Image> ToMainColorsAsync(Image image)
        {
            Bitmap picture = new Bitmap(image);

            Task<Bitmap> task = ChangePixelAsync(picture);
            picture = await task;
            return picture;
        }

        private Bitmap ChangePixel(Bitmap picture)
        {
            for (int x = 0; x < picture.Width; x++)
            {
                for (int y = 0; y < picture.Height; y++)
                {
                    Color pixelColor = picture.GetPixel(x, y);
                    var r = pixelColor.R;
                    var g = pixelColor.G;
                    var b = pixelColor.B;

                    if (r > g && r > b)
                    {
                        Color newColor = ColorTranslator.FromHtml("#FF0000");
                        picture.SetPixel(x, y, newColor);
                    }
                    else if (g > r && g > b)
                    {
                        Color newColor = ColorTranslator.FromHtml("#00FF00");
                        picture.SetPixel(x, y, newColor);
                    }
                    else if (b > r && b > g)
                    {
                        Color newColor = ColorTranslator.FromHtml("#0000FF");
                        picture.SetPixel(x, y, newColor);
                    }
                }
            }

            return picture;
        }

        private async Task<Bitmap> ChangePixelAsync(Bitmap picture)
        {
            for (int x = 0; x < picture.Width; x++)
            {
                for (int y = 0; y < picture.Height; y++)
                {
                    Color pixelColor = picture.GetPixel(x, y);
                    var r = pixelColor.R;
                    var g = pixelColor.G;
                    var b = pixelColor.B;

                    if (r > g && r > b)
                    {
                        Color newColor = ColorTranslator.FromHtml("#FF0000");
                        picture.SetPixel(x, y, newColor);
                    }
                    else if (g > r && g > b)
                    {
                        Color newColor = ColorTranslator.FromHtml("#00FF00");
                        picture.SetPixel(x, y, newColor);
                    }
                    else if (b > r && b > g)
                    {
                        Color newColor = ColorTranslator.FromHtml("#0000FF");
                        picture.SetPixel(x, y, newColor);
                    }
                }
            }

            return picture;
        }

        public void Save(Image image, string path)
        {
            image.Save(path);
            image.Dispose();
        }
    }
}
