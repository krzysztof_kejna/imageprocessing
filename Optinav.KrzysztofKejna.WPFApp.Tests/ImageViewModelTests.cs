using Microsoft.Win32;
using Moq;
using NUnit.Framework;
using Optinav.KrzysztofKejna.ImageProcessingLibrary;
using Optinav.KrzysztofKejna.WPFApp.ViewModel;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Media;

namespace Optinav.KrzysztofKejna.WPFApp.Tests
{
    public class ImageViewModelTests
    {
        [Test]
        public void ImageDominantColorIsBlue_ImageIsBlue()
        {
            ImageViewModel iViewModel = new ImageViewModel();
            string filePath = Path.GetFullPath(@"..\..\..\");
            iViewModel.FileExtension = ".jpg";
            iViewModel.FilePath = filePath + "DominantColorIsBlue.jpg";
            iViewModel.OnChangeAsync();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImgAsync.jpg";
            var image = new Bitmap(temporaryPath);
            Assert.IsTrue(image.Palette.Entries.All(x => x == System.Drawing.Color.Blue));
        }

        [Test]
        public void ImageDominantColorIsRed_ImageIsRed()
        {
            ImageViewModel iViewModel = new ImageViewModel();
            string filePath = Path.GetFullPath(@"..\..\..\");
            iViewModel.FileExtension = ".jpg";
            iViewModel.FilePath = filePath + "DominantColorIsRed.jpg";
            iViewModel.OnChange();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImg.jpg";
            var image = new Bitmap(temporaryPath);
            Assert.IsTrue(image.Palette.Entries.All(x => x == System.Drawing.Color.Red));
        }

        [Test]
        public void ImageDominantColorIsGreen_ImageIsGreen()
        {
            ImageViewModel iViewModel = new ImageViewModel();
            string filePath = Path.GetFullPath(@"..\..\..\");
            iViewModel.FileExtension = ".jpg";
            iViewModel.FilePath = filePath + "DominantColorIsGreen.jpg";
            iViewModel.OnChange();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImg.jpg";
            var image = new Bitmap(temporaryPath);
            Assert.IsTrue(image.Palette.Entries.All(x => x == System.Drawing.Color.Green));
        }

        [Test]
        public void ImageColorIsBlack_ImageIsBlack()
        {
            ImageViewModel iViewModel = new ImageViewModel();
            string filePath = Path.GetFullPath(@"..\..\..\");
            iViewModel.FileExtension = ".jpg";
            iViewModel.FilePath = filePath + "ColorIsBlack.jpg";
            iViewModel.OnChange();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImg.jpg";
            var image = new Bitmap(temporaryPath);
            Assert.IsTrue(image.Palette.Entries.All(x => x == System.Drawing.Color.Black));
        }
    }
}