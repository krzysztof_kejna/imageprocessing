﻿using Microsoft.Win32;
using Optinav.KrzysztofKejna.ImageProcessingLibrary;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace Optinav.KrzysztofKejna.WPFApp.ViewModel
{
    public class ImageViewModel : INotifyPropertyChanged
    {
        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                OnPropertyChanged("filePath");

            }
        }

        private string fileExtension;
        public string FileExtension
        {
            get { return fileExtension; }
            set
            {
                fileExtension = value;
                OnPropertyChanged("fileExtension");

            }
        }

        private TimeSpan timePass;
        public TimeSpan TimePass { get { return timePass; } set
            {
                timePass = value;
                OnPropertyChanged("timePass");

            } 
        }

        private TimeSpan timePassAsync;
        public TimeSpan TimePassAsync
        {
            get { return timePassAsync; }
            set
            {
                timePassAsync = value;
                OnPropertyChanged("timePassAsync");

            }
        }

        private BitmapImage image;
        public BitmapImage Image { get { return image; } set {
                image = value;
                OnPropertyChanged("image");
            } }

        public MyICommand BrowseCommand { get; set; }
        public MyICommand ChangeCommand { get; set; }
        public MyICommand ChangeCommandAsync { get; set; }

        public ImageViewModel()
        {
            BrowseCommand = new MyICommand(OnBrowse);
            ChangeCommand = new MyICommand(OnChange);
            ChangeCommandAsync = new MyICommand(OnChangeAsync);
        }

        private void OnBrowse()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png;*.bmp;|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png|" +
              "Bitmap (*.bmp)|*.bmp";
            if (op.ShowDialog() == true)
            {
                filePath = op.FileName;
                fileExtension = Path.GetExtension(op.FileName);
                BindImage(filePath);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void OnChange()
        {
            Image = new BitmapImage();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImg" + fileExtension;
            Stopwatch stopWatch = new Stopwatch();
            ImageProcessing imageProcessing = new ImageProcessing();
            if(filePath == null)
            {
                return;
            }
            var picture = new Bitmap(filePath);
            stopWatch.Start();
            var newImage = imageProcessing.ToMainColors(picture);
            stopWatch.Stop();
            TimePass = stopWatch.Elapsed;
            imageProcessing.Save(newImage, temporaryPath);
            BindImage(temporaryPath);
        }

        public void OnChangeAsync()
        {
            Image = new BitmapImage();
            string temporaryFolder = Path.GetTempPath();
            string temporaryPath = temporaryFolder + "tempImgAsync" + fileExtension;
            Stopwatch stopWatch = new Stopwatch();
            ImageProcessing imageProcessing = new ImageProcessing();
            if (filePath == null)
            {
                return;
            }
            var picture = new Bitmap(filePath);
            stopWatch.Start();
            var newImage = imageProcessing.ToMainColorsAsync(picture).Result;
            stopWatch.Stop();
            TimePassAsync = stopWatch.Elapsed;
            imageProcessing.Save(newImage, temporaryPath);
            BindImage(temporaryPath);
        }


        private void BindImage(string path)
        {
            Image = new BitmapImage();
            Image.BeginInit();
            Image.CacheOption = BitmapCacheOption.OnLoad;
            Image.UriSource = new Uri(path);
            Image.EndInit();            
        }
    }
}
